export const XS_SCREEN = 600;
export const SM_SCREEN = 960;
export const MD_SCREEN = 1280;
export const LG_SCREEN = 1920;

export const HEAD_H = 64;
export const HEAD_H_XS = 112;
export const FOOT_H = 177;
