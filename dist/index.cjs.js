'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var PropTypes = require('prop-types');
var clsx = require('clsx');
var core = require('@material-ui/core');
var styles = require('@material-ui/core/styles');
var reactI18next = require('react-i18next');
var CloseIcon = require('@material-ui/icons/Close');
var Icon = require('@mdi/react');
var js = require('@mdi/js');
var MuiAlert = require('@material-ui/lab/Alert');
var colors = require('@material-ui/core/colors');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);
var PropTypes__default = /*#__PURE__*/_interopDefaultLegacy(PropTypes);
var clsx__default = /*#__PURE__*/_interopDefaultLegacy(clsx);
var CloseIcon__default = /*#__PURE__*/_interopDefaultLegacy(CloseIcon);
var Icon__default = /*#__PURE__*/_interopDefaultLegacy(Icon);
var MuiAlert__default = /*#__PURE__*/_interopDefaultLegacy(MuiAlert);

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);

    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }

    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var useStyles$3 = styles.makeStyles(function (theme) {
  return {
    card: {
      'marginTop': theme.spacing(2),
      '&:first-child': {
        marginTop: theme.spacing(0)
      },
      'flexGrow': 1,
      '& .MuiCardHeader-root': {
        borderBottom: '1px solid',
        borderBottomColor: theme.palette.divider
      }
    },
    cardActions: {
      borderTop: '1px solid',
      borderTopColor: theme.palette.divider
    },
    cardActionsError: {
      borderTop: '1px solid',
      borderTopColor: theme.palette.error.light
    },
    cardError: {
      'border': '1px solid',
      'borderColor': theme.palette.error.light,
      '& .MuiCardHeader-root': {
        color: theme.palette.error.main,
        borderBottom: '1px solid',
        borderBottomColor: theme.palette.error.light
      }
    }
  };
});
function Card(props) {
  var _clsx2;

  var classes = useStyles$3();

  var errorHeaderMsg = function errorHeaderMsg() {
    if (props.error && props.totalErrors === 1) {
      return /*#__PURE__*/React__default["default"].createElement(core.Typography, {
        variant: "caption",
        component: "p"
      }, "1 error");
    } else if (props.error && props.totalErrors > 99) {
      return /*#__PURE__*/React__default["default"].createElement(core.Typography, {
        variant: "caption",
        component: "p"
      }, "99+ errors");
    } else if (props.error && props.totalErrors > 1 && props.totalErrors <= 99) {
      return /*#__PURE__*/React__default["default"].createElement(core.Typography, {
        variant: "caption",
        component: "p"
      }, props.totalErrors, " errors");
    }
  };

  return /*#__PURE__*/React__default["default"].createElement(core.Card, {
    className: clsx__default["default"](classes.card, _defineProperty({}, classes.cardError, props.error)),
    variant: "outlined"
  }, /*#__PURE__*/React__default["default"].createElement(core.CardHeader, {
    title: /*#__PURE__*/React__default["default"].createElement(core.Typography, {
      variant: "subtitle2",
      component: props.titleComponent
    }, props.title),
    subheader: errorHeaderMsg()
  }), props.content && /*#__PURE__*/React__default["default"].createElement(core.CardContent, null, /*#__PURE__*/React__default["default"].createElement(core.Grid, {
    container: true
  }, /*#__PURE__*/React__default["default"].createElement(core.Grid, {
    item: true,
    xs: 12
  }, props.content))), (props.primaryButton || props.secondaryButton) && /*#__PURE__*/React__default["default"].createElement(core.CardActions, {
    className: clsx__default["default"]((_clsx2 = {}, _defineProperty(_clsx2, classes.cardActions, props.error === false), _defineProperty(_clsx2, classes.cardActionsError, props.error === true), _clsx2))
  }, /*#__PURE__*/React__default["default"].createElement(core.Button, {
    color: "primary",
    onClick: props.primaryClick
  }, props.primaryButton), props.secondaryButton && /*#__PURE__*/React__default["default"].createElement(core.Button, {
    color: "primary",
    onClick: props.secondaryClick
  }, props.secondaryButton)));
}
Card.propTypes = {
  /**
    The title of the card header.
  */
  title: PropTypes__default["default"].string.isRequired,

  /**
    The component used for the root node of the card title.
   */
  titleComponent: PropTypes__default["default"].elementType,

  /**
    Toggles the error state of the card.
  */
  error: PropTypes__default["default"].bool,

  /**
    The amount of errors.
  */
  totalErrors: PropTypes__default["default"].number,

  /**
  The content of the card body.
  */
  content: PropTypes__default["default"].element.isRequired,

  /**
    The label text of the primary button.
  */
  primaryButton: PropTypes__default["default"].string,

  /**
    The label text of the secondary button.
  */
  secondaryButton: PropTypes__default["default"].string,

  /**
  The function of the primary button.
  */
  primaryClick: PropTypes__default["default"].func,

  /**
  The function of the secondary button.
  */
  secondaryClick: PropTypes__default["default"].func
};
Card.defaultProps = {
  titleComponent: 'h3',
  error: false,
  totalErrors: 0
};

var useStyles$2 = styles.makeStyles(function (theme) {
  return {
    dialogTitle: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: theme.spacing(1.5, 3),
      borderBottom: '1px solid',
      borderBottomColor: theme.palette.divider
    },
    dialogContent: {
      padding: theme.spacing(3)
    },
    dialogFooter: {
      padding: theme.spacing(1.75, 3),
      borderTop: '1px solid',
      borderTopColor: theme.palette.divider
    },
    btnGroup: {
      '& button': {
        marginLeft: theme.spacing(2)
      }
    }
  };
});
function Dialog(props) {
  var classes = useStyles$2();

  var _useTranslation = reactI18next.useTranslation(),
      t = _useTranslation.t;

  var toggleDialog = props.toggleDialog,
      open = props.open,
      id = props.id,
      title = props.title,
      content = props.content,
      primaryButton = props.primaryButton,
      secondaryButton = props.secondaryButton,
      thirdButton = props.thirdButton,
      backButton = props.backButton,
      handlePrimaryClick = props.handlePrimaryClick,
      handleSecondaryClick = props.handleSecondaryClick,
      handleThirdClick = props.handleThirdClick,
      handleBackClick = props.handleBackClick;
  return /*#__PURE__*/React__default["default"].createElement(React__default["default"].Fragment, null, /*#__PURE__*/React__default["default"].createElement(core.Dialog, {
    classes: {
      paper: classes.dialogPaper
    },
    onClose: toggleDialog,
    "aria-labelledby": id,
    open: open,
    maxWidth: "sm",
    fullWidth: true,
    disableBackdropClick: true
  }, /*#__PURE__*/React__default["default"].createElement(core.DialogTitle, {
    id: id,
    className: classes.dialogTitle,
    disableTypography: true
  }, /*#__PURE__*/React__default["default"].createElement(core.Grid, {
    container: true,
    alignItems: "center"
  }, backButton && /*#__PURE__*/React__default["default"].createElement(core.Grid, {
    item: true
  }, /*#__PURE__*/React__default["default"].createElement(core.IconButton, {
    className: "mr-1",
    "aria-label": backButton,
    edge: "start",
    onClick: handleBackClick,
    onKeyPress: function onKeyPress(e) {
      e.preventDefault();

      if (e.key === 'Enter') {
        handleBackClick();
      }
    }
  }, /*#__PURE__*/React__default["default"].createElement(Icon__default["default"], {
    path: js.mdiArrowLeft,
    size: 1
  }))), /*#__PURE__*/React__default["default"].createElement(core.Grid, {
    item: true
  }, /*#__PURE__*/React__default["default"].createElement(core.Typography, {
    component: "h2",
    variant: "h6"
  }, title))), /*#__PURE__*/React__default["default"].createElement(core.IconButton, {
    "aria-label": t('Close'),
    onClick: toggleDialog,
    edge: "end",
    onKeyPress: function onKeyPress(e) {
      e.preventDefault();

      if (e.key === 'Enter') {
        toggleDialog(e);
      }
    }
  }, /*#__PURE__*/React__default["default"].createElement(CloseIcon__default["default"], null))), /*#__PURE__*/React__default["default"].createElement(core.DialogContent, null, /*#__PURE__*/React__default["default"].createElement("div", {
    className: classes.dialogContent
  }, content)), secondaryButton || primaryButton ? /*#__PURE__*/React__default["default"].createElement(core.DialogActions, {
    className: classes.dialogFooter
  }, /*#__PURE__*/React__default["default"].createElement(core.Grid, {
    container: true,
    justify: "space-between"
  }, /*#__PURE__*/React__default["default"].createElement(core.Grid, {
    item: true
  }, thirdButton && /*#__PURE__*/React__default["default"].createElement(core.Button, {
    className: clsx__default["default"](classes.thirdButton, 'MuiIconButton-edgeStart'),
    color: "primary",
    onKeyPress: function onKeyPress(e) {
      e.preventDefault();

      if (e.key === 'Enter') {
        handleThirdClick(e);
      }
    },
    onClick: handleThirdClick
  }, thirdButton)), /*#__PURE__*/React__default["default"].createElement(core.Grid, {
    item: true,
    className: classes.btnGroup
  }, secondaryButton && /*#__PURE__*/React__default["default"].createElement(core.Button, {
    className: classes.secondaryButton,
    variant: "outlined",
    color: "primary",
    onKeyPress: function onKeyPress(e) {
      e.preventDefault();

      if (e.key === 'Enter') {
        handleSecondaryClick(e);
      }
    },
    onClick: handleSecondaryClick
  }, secondaryButton), primaryButton && /*#__PURE__*/React__default["default"].createElement(core.Button, {
    variant: "contained",
    color: "primary",
    onKeyPress: function onKeyPress(e) {
      e.preventDefault();

      if (e.key === 'Enter') {
        handlePrimaryClick(e);
      }
    },
    onClick: handlePrimaryClick
  }, primaryButton)))) : ''));
}
Dialog.propTypes = {
  /**
   Dialog id used for a11y
  */
  id: PropTypes__default["default"].string.isRequired,

  /**
   Dialogs default state, true = open, false = closed
  */
  open: PropTypes__default["default"].bool,

  /**
   Dialog header text
   */
  title: PropTypes__default["default"].string.isRequired,

  /**
   Dialog content
   */
  content: PropTypes__default["default"].node.isRequired,

  /**
   Text for primary button
   */
  primaryButton: PropTypes__default["default"].string,

  /**
   Text for secondary button
   */
  secondaryButton: PropTypes__default["default"].string,

  /**
   Text for third button
   */
  thirdButton: PropTypes__default["default"].string,

  /**
   Text for back button
   */
  backButton: PropTypes__default["default"].string,

  /**
   Click handler for primary action
   */
  handlePrimaryClick: PropTypes__default["default"].func,

  /**
   Click handler for secondary action
   */
  handleSecondaryClick: PropTypes__default["default"].func,

  /**
   Click handler for third action
   */
  handleThirdClick: PropTypes__default["default"].func,

  /**
   Click handler for back button
   */
  handleBackClick: PropTypes__default["default"].func
};
Dialog.defaultProps = {
  open: false
};

var DRAWER_WIDTH = 400;
var useStyles$1 = styles.makeStyles(function (theme) {
  return {
    drawer: {
      '& .MuiDrawer-paper': {
        width: DRAWER_WIDTH,
        boxSizing: 'border-box'
      }
    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: theme.palette.common.white,
      margin: theme.spacing(0, -3, 3, 0),
      boxShadow: theme.shadows[0],
      borderBottom: '1px solid',
      borderBottomColor: theme.palette.divider,
      position: 'fixed',
      top: 0,
      zIndex: 500,
      width: DRAWER_WIDTH,
      boxSizing: 'border-box',
      padding: theme.spacing(1.5, 3)
    },
    drawerContent: {
      marginTop: theme.spacing(8),
      marginBottom: theme.spacing(8),
      padding: theme.spacing(3),
      overflowY: 'auto',
      overflowX: 'hidden',
      height: '100%'
    },
    drawerSection: {
      paddingTop: theme.spacing(2),
      flexShrink: 0
    },
    footer: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginRight: theme.spacing(-3),
      padding: theme.spacing(1.75, 3),
      borderTop: '1px solid',
      borderTopColor: theme.palette.divider,
      position: 'fixed',
      bottom: 0,
      width: DRAWER_WIDTH,
      boxSizing: 'border-box',
      backgroundColor: theme.palette.common.white,
      zIndex: 500
    },
    btnGroup: {
      '& button': {
        marginLeft: theme.spacing(2)
      }
    }
  };
});
function Drawer(props) {
  var classes = useStyles$1();

  var _useTranslation = reactI18next.useTranslation(),
      t = _useTranslation.t;

  var content = function content() {
    return /*#__PURE__*/React__default["default"].createElement(React__default["default"].Fragment, null, /*#__PURE__*/React__default["default"].createElement("div", {
      className: classes.drawerHeader
    }, /*#__PURE__*/React__default["default"].createElement(core.Typography, {
      component: "h2",
      variant: "h6"
    }, props.title), /*#__PURE__*/React__default["default"].createElement(core.IconButton, {
      onClick: props.toggleDrawer,
      edge: "end",
      "aria-label": t('Close')
    }, /*#__PURE__*/React__default["default"].createElement(CloseIcon__default["default"], null))), /*#__PURE__*/React__default["default"].createElement("div", {
      className: classes.drawerContent
    }, props.content), /*#__PURE__*/React__default["default"].createElement("div", {
      className: classes.footer
    }, /*#__PURE__*/React__default["default"].createElement(core.Grid, {
      container: true,
      justify: "space-between"
    }, /*#__PURE__*/React__default["default"].createElement(core.Grid, {
      item: true
    }, props.thirdButton && /*#__PURE__*/React__default["default"].createElement(core.Button, {
      className: clsx__default["default"](classes.thirdButton, 'MuiIconButton-edgeStart'),
      color: "primary",
      onClick: props.handleThirdClick
    }, props.thirdButton)), /*#__PURE__*/React__default["default"].createElement(core.Grid, {
      item: true,
      className: classes.btnGroup
    }, props.secondaryButton && /*#__PURE__*/React__default["default"].createElement(core.Button, {
      variant: "outlined",
      color: "primary",
      onClick: props.handleSecondaryClick
    }, props.secondaryButton), props.primaryButton && /*#__PURE__*/React__default["default"].createElement(core.Button, {
      variant: "contained",
      color: "primary",
      onClick: props.handlePrimaryClick
    }, props.primaryButton)))));
  };

  return /*#__PURE__*/React__default["default"].createElement(React__default["default"].Fragment, null, /*#__PURE__*/React__default["default"].createElement(core.Drawer, {
    anchor: "right",
    open: props.open,
    onEscapeKeyDown: props.toggleDrawer,
    onBackdropClick: function onBackdropClick(e) {
      e.preventDefault();
    },
    variant: "temporary",
    className: classes.drawer
  }, content()));
}
Drawer.propTypes = {
  /**
   Drawers default state, true = open, false = closed
  */
  open: PropTypes__default["default"].bool,

  /**
   Drawer header text
   */
  title: PropTypes__default["default"].string.isRequired,

  /**
   Drawer content
   */
  content: PropTypes__default["default"].node.isRequired,

  /**
   Text for primary button
   */
  primaryButton: PropTypes__default["default"].string.isRequired,

  /**
   Text for secondary button
   */
  secondaryButton: PropTypes__default["default"].string,

  /**
   Text for third button
   */
  thirdButton: PropTypes__default["default"].string,

  /**
   Click handler for primary action
   */
  handlePrimaryClick: PropTypes__default["default"].func.isRequired,

  /**
   Click handler for secondary action
   */
  handleSecondaryClick: PropTypes__default["default"].func,

  /**
   Click handler for third action
   */
  handleThirdClick: PropTypes__default["default"].func
};
Drawer.defaultProps = {
  open: false
};

// const useStyles = makeStyles((theme) => ({
//   root: {
//     'minWidth': theme.spacing(49),
//     '& .MuiAlert-root': {
//       width: '100%',
//     },
//     [theme.breakpoints.down('xs')]: {
//       'bottom': theme.spacing(10),
//       'minWidth': '0',
//       '& .MuiAlert-root': {
//         width: '100%',
//       },
//     },
//   },
// }));

function Alert(props) {
  return /*#__PURE__*/React__default["default"].createElement(MuiAlert__default["default"], _extends({
    elevation: 6,
    variant: "filled"
  }, props));
}

function Snackbar(props) {
  // const classes = useStyles();
  return /*#__PURE__*/React__default["default"].createElement(core.Snackbar, {
    role: "none",
    open: props.open,
    autoHideDuration: 6000,
    onClose: props.handleClose,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'left'
    } // className={classes.root}

  }, /*#__PURE__*/React__default["default"].createElement(Alert, {
    role: "none",
    onClose: props.handleClose,
    severity: props.severity
  }, /*#__PURE__*/React__default["default"].createElement("span", {
    role: "alert"
  }, props.message)));
}
var SEVERITY = {
  ERROR: 'error',
  INFO: 'info',
  SUCCESS: 'success',
  WARNING: 'warning'
};
Snackbar.propTypes = {
  /**
   * The message to display.
   */
  message: PropTypes__default["default"].node.isRequired,

  /**
    If true, Snackbar is open.
  */
  open: PropTypes__default["default"].bool,

  /**
    The severity of the alert. This defines the color and icon used.
  */
  severity: PropTypes__default["default"].oneOf(Object.values(SEVERITY)).isRequired,

  /**
    Click handler to close snackbar
  */
  handleClose: PropTypes__default["default"].func.isRequired
};
Snackbar.defaultProps = {
  open: false
};

var props = {
  MuiButton: {
    disableFocusRipple: true
  },
  MuiIconButton: {
    disableFocusRipple: true
  },
  MuiFormControl: {
    fullWidth: true,
    margin: 'dense',
    variant: 'outlined'
  },
  MuiTextField: {
    margin: 'dense',
    variant: 'outlined',
    rows: 3,
    rowsMax: 100
  },
  MuiSelect: {
    MenuProps: {
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'left'
      },
      getContentAnchorEl: null
    }
  }
};
var shadows = ['none', '0px 2px 1px -1px rgba(117,117,117,0.2),0px 1px 1px 0px rgba(117,117,117,0.14),0px 1px 3px 0px rgba(117,117,117,0.12)', '0px 3px 1px -2px rgba(117,117,117,0.2),0px 2px 2px 0px rgba(117,117,117,0.14),0px 1px 5px 0px rgba(117,117,117,0.12)', '0px 3px 3px -2px rgba(117,117,117,0.2),0px 3px 4px 0px rgba(117,117,117,0.14),0px 1px 8px 0px rgba(117,117,117,0.12)', '0px 2px 4px -1px rgba(117,117,117,0.2),0px 4px 5px 0px rgba(117,117,117,0.14),0px 1px 10px 0px rgba(117,117,117,0.12)', '0px 3px 5px -1px rgba(117,117,117,0.2),0px 5px 8px 0px rgba(117,117,117,0.14),0px 1px 14px 0px rgba(117,117,117,0.12)', '0px 3px 5px -1px rgba(117,117,117,0.2),0px 6px 10px 0px rgba(117,117,117,0.14),0px 1px 18px 0px rgba(117,117,117,0.12)', '0px 4px 5px -2px rgba(117,117,117,0.2),0px 7px 10px 1px rgba(117,117,117,0.14),0px 2px 16px 1px rgba(117,117,117,0.12)', '0px 5px 5px -3px rgba(117,117,117,0.2),0px 8px 10px 1px rgba(117,117,117,0.14),0px 3px 14px 2px rgba(117,117,117,0.12)', '0px 5px 6px -3px rgba(117,117,117,0.2),0px 9px 12px 1px rgba(117,117,117,0.14),0px 3px 16px 2px rgba(117,117,117,0.12)', '0px 6px 6px -3px rgba(117,117,117,0.2),0px 10px 14px 1px rgba(117,117,117,0.14),0px 4px 18px 3px rgba(117,117,117,0.12)', '0px 6px 7px -4px rgba(117,117,117,0.2),0px 11px 15px 1px rgba(117,117,117,0.14),0px 4px 20px 3px rgba(117,117,117,0.12)', '0px 7px 8px -4px rgba(117,117,117,0.2),0px 12px 17px 2px rgba(117,117,117,0.14),0px 5px 22px 4px rgba(117,117,117,0.12)', '0px 7px 8px -4px rgba(117,117,117,0.2),0px 13px 19px 2px rgba(117,117,117,0.14),0px 5px 24px 4px rgba(117,117,117,0.12)', '0px 7px 9px -4px rgba(117,117,117,0.2),0px 14px 21px 2px rgba(117,117,117,0.14),0px 5px 26px 4px rgba(117,117,117,0.12)', '0px 8px 9px -5px rgba(117,117,117,0.2),0px 15px 22px 2px rgba(117,117,117,0.14),0px 6px 28px 5px rgba(117,117,117,0.12)', '0px 8px 10px -5px rgba(117,117,117,0.2),0px 16px 24px 2px rgba(117,117,117,0.14),0px 6px 30px 5px rgba(117,117,117,0.12)', '0px 8px 11px -5px rgba(117,117,117,0.2),0px 17px 26px 2px rgba(117,117,117,0.14),0px 6px 32px 5px rgba(117,117,117,0.12)', '0px 9px 11px -5px rgba(117,117,117,0.2),0px 18px 28px 2px rgba(117,117,117,0.14),0px 7px 34px 6px rgba(117,117,117,0.12)', '0px 9px 12px -6px rgba(117,117,117,0.2),0px 19px 29px 2px rgba(117,117,117,0.14),0px 7px 36px 6px rgba(117,117,117,0.12)', '0px 10px 13px -6px rgba(117,117,117,0.2),0px 20px 31px 3px rgba(117,117,117,0.14),0px 8px 38px 7px rgba(117,117,117,0.12)', '0px 10px 13px -6px rgba(117,117,117,0.2),0px 21px 33px 3px rgba(117,117,117,0.14),0px 8px 40px 7px rgba(117,117,117,0.12)', '0px 10px 14px -6px rgba(117,117,117,0.2),0px 22px 35px 3px rgba(117,117,117,0.14),0px 8px 42px 7px rgba(117,117,117,0.12)', '0px 11px 14px -7px rgba(117,117,117,0.2),0px 23px 36px 3px rgba(117,117,117,0.14),0px 9px 44px 8px rgba(117,117,117,0.12)', '0px 11px 15px -7px rgba(117,117,117,0.2),0px 24px 38px 3px rgba(117,117,117,0.14),0px 9px 46px 8px rgba(117,117,117,0.12)'];
var typography = {
  fontFamily: ['"Roboto"', 'sans-serif'],
  h1: {
    letterSpacing: '-0.01562em'
  },
  h2: {
    letterSpacing: '-0.00833em'
  },
  h3: {
    letterSpacing: '0em'
  },
  h4: {
    letterSpacing: '0.00735em'
  },
  h5: {
    letterSpacing: '0em'
  },
  h6: {
    letterSpacing: '0.0075em'
  },
  subtitle1: {
    letterSpacing: '0.00938em'
  },
  subtitle2: {
    letterSpacing: '0.00714em'
  },
  body1: {
    letterSpacing: '0.00938em'
  },
  body2: {
    letterSpacing: '0.01071em'
  },
  button: {
    letterSpacing: '0.02857em'
  },
  caption: {
    letterSpacing: '0.03333em'
  },
  overline: {
    letterSpacing: '0.08333em'
  }
};
var theme = core.createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#1A73E8',
      light: '#d4e5ff',
      dark: '#1765CC'
    },
    secondary: {
      main: '#34dbc5',
      light: '#77fff8',
      dark: '#00a995'
    },
    error: {
      main: '#E91B0C'
    },
    buttons: {
      default: '#5F6368',
      defaultHover: '#54575C'
    }
  },
  props: props,
  shadows: shadows,
  typography: typography
});
theme.overrides = {
  // BUTTONS
  MuiButton: {
    label: {
      textTransform: 'none'
    },
    endIcon: {
      marginRight: theme.spacing(-1)
    },
    startIcon: {
      marginLeft: theme.spacing(-1)
    },
    // contained
    contained: {
      'boxShadow': theme.shadows[0],
      // contained disabled
      '&$disabled': {
        color: styles.fade(theme.palette.common.black, 0.4),
        backgroundColor: styles.fade(theme.palette.common.black, 0.08)
      }
    },
    // contained primary
    containedPrimary: {
      '&:hover': {
        backgroundColor: theme.palette.primary.dark,
        boxShadow: theme.shadows[0]
      },
      '&$focusVisible': {
        backgroundColor: theme.palette.primary.dark,
        boxShadow: theme.shadows[0]
      }
    },
    // outlined
    outlined: {
      // outlined disabled
      '&$disabled': {
        color: styles.fade(theme.palette.common.black, 0.4),
        borderColor: styles.fade(theme.palette.common.black, 0.12)
      }
    },
    // outlined primary
    outlinedPrimary: {
      'borderColor': styles.fade(theme.palette.primary.main, 0.4),
      '&:hover': {
        color: theme.palette.primary.dark,
        borderColor: styles.fade(theme.palette.primary.main, 0.4),
        backgroundColor: styles.fade(theme.palette.primary.main, 0.12)
      },
      '&$focusVisible': {
        color: theme.palette.primary.dark,
        backgroundColor: styles.fade(theme.palette.primary.main, 0.12)
      }
    },
    // text
    text: {
      'color': theme.palette.buttons.default,
      '& $startIcon': {
        color: theme.palette.buttons.default,
        marginLeft: theme.spacing(0)
      },
      '& $endIcon': {
        color: theme.palette.buttons.default,
        marginRight: theme.spacing(0)
      },
      '&:hover': {
        'backgroundColor': styles.fade(theme.palette.buttons.default, 0.12),
        'color': theme.palette.buttons.defaultHover,
        '& $startIcon': {
          color: theme.palette.buttons.defaultHover
        },
        '& $endIcon': {
          color: theme.palette.buttons.defaultHover
        }
      },
      '&$focusVisible': {
        'backgroundColor': styles.fade(theme.palette.buttons.default, 0.12),
        'color': theme.palette.buttons.defaultHover,
        '& $startIcon': {
          color: theme.palette.buttons.defaultHover
        },
        '& $endIcon': {
          color: theme.palette.buttons.defaultHover
        }
      },
      // text disabled
      '&$disabled': {
        'color': styles.fade(theme.palette.common.black, 0.4),
        '& $startIcon': {
          color: [styles.fade(theme.palette.common.black, 0.4), '!important']
        },
        '& $endIcon': {
          color: [styles.fade(theme.palette.common.black, 0.4), '!important']
        }
      }
    },
    // text primary
    textPrimary: {
      '& $startIcon': {
        color: theme.palette.primary.main
      },
      '& $endIcon': {
        color: theme.palette.primary.main
      },
      '&:hover': {
        'color': theme.palette.primary.dark,
        'backgroundColor': styles.fade(theme.palette.primary.main, 0.12),
        '& $startIcon': {
          color: theme.palette.primary.dark
        },
        '& $endIcon': {
          color: theme.palette.primary.dark
        }
      },
      '&$focusVisible': {
        'color': theme.palette.primary.dark,
        'backgroundColor': styles.fade(theme.palette.primary.main, 0.12),
        '& $startIcon': {
          color: theme.palette.primary.dark
        },
        '& $endIcon': {
          color: theme.palette.primary.dark
        }
      }
    },
    disabled: {},
    focusVisible: {}
  },
  // ICON BUTTONS
  MuiIconButton: {
    edgeStart: {
      marginLeft: theme.spacing(-1)
    },
    edgeEnd: {
      marginRight: theme.spacing(-1)
    },
    root: {
      'color': theme.palette.buttons.default,
      'padding': theme.spacing(1),
      '&:hover': {
        backgroundColor: styles.fade(theme.palette.buttons.default, 0.12),
        color: theme.palette.buttons.defaultHover
      },
      '&$focusVisible': {
        backgroundColor: styles.fade(theme.palette.buttons.default, 0.12),
        color: theme.palette.buttons.defaultHover
      },
      // disabled icon
      '&$disabled': {
        color: styles.fade(theme.palette.common.black, 0.4)
      }
    },
    // primary icon
    colorPrimary: {
      '&:hover': {
        color: theme.palette.primary.dark,
        backgroundColor: styles.fade(theme.palette.primary.main, 0.12)
      },
      '&$focusVisible': {
        color: theme.palette.primary.dark,
        backgroundColor: styles.fade(theme.palette.primary.main, 0.12)
      }
    },
    disabled: {},
    focusVisible: {}
  },
  MuiTouchRipple: {
    'rippleVisible': {
      opacity: 0.2,
      animation: "$enter 550ms ".concat(theme.transitions.easing.easeInOut)
    },
    '@keyframes enter': {
      '0%': {
        transform: 'scale(0)',
        opacity: 0.1
      },
      '100%': {
        transform: 'scale(1)',
        opacity: 0.2
      }
    }
  },
  MuiTooltip: {
    tooltip: {
      fontSize: '0.75rem',
      backgroundColor: '#616161E6'
    }
  }
};
var darkTheme = core.createMuiTheme(_objectSpread2(_objectSpread2({}, theme), {}, {
  palette: _objectSpread2(_objectSpread2({}, theme.palette), {}, {
    type: 'dark',
    background: {
      paper: '#173048'
    },
    text: {
      primary: theme.palette.common.white,
      secondary: styles.fade(theme.palette.common.white, 0.7)
    },
    divider: styles.fade(theme.palette.common.white, 0.12)
  }),
  overrides: _objectSpread2(_objectSpread2({}, theme.overrides), {}, {
    MuiButton: _objectSpread2(_objectSpread2({}, theme.overrides.MuiButton), {}, {
      contained: _objectSpread2(_objectSpread2({}, theme.overrides.MuiButton.contained), {}, {
        // contained disabled
        '&$disabled': {
          color: styles.fade(theme.palette.common.white, 0.4),
          backgroundColor: styles.fade(theme.palette.common.white, 0.08)
        }
      }),
      // outlined
      outlined: _objectSpread2(_objectSpread2({}, theme.overrides.MuiButton.outlined), {}, {
        'color': theme.palette.common.white,
        'borderColor': styles.fade(theme.palette.common.white, 0.4),
        '&:hover': {
          backgroundColor: styles.fade(theme.palette.common.white, 0.12)
        },
        '&$focusVisible': {
          backgroundColor: styles.fade(theme.palette.common.white, 0.12)
        },
        // outlined disabled
        '&$disabled': {
          color: styles.fade(theme.palette.common.white, 0.4),
          borderColor: styles.fade(theme.palette.common.white, 0.12)
        }
      }),
      // text
      text: _objectSpread2(_objectSpread2({}, theme.overrides.MuiButton.text), {}, {
        'color': theme.palette.common.white,
        '& $startIcon': {
          color: theme.palette.common.white,
          marginLeft: theme.spacing(0)
        },
        '& $endIcon': {
          color: theme.palette.common.white,
          marginRight: theme.spacing(0)
        },
        '&:hover': {
          backgroundColor: styles.fade(theme.palette.common.white, 0.12)
        },
        '&$focusVisible': {
          backgroundColor: styles.fade(theme.palette.common.white, 0.12)
        },
        // text disabled
        '&$disabled': {
          'color': styles.fade(theme.palette.common.white, 0.4),
          '& $startIcon': {
            color: [styles.fade(theme.palette.common.white, 0.4), '!important']
          },
          '& $endIcon': {
            color: [styles.fade(theme.palette.common.white, 0.4), '!important']
          }
        }
      }),
      startIcon: {},
      endIcon: {},
      disabled: {},
      focusVisible: {}
    })
  })
}));

var useStyles = styles.makeStyles({
  '@global': {
    // ************* Overrides **************
    // Breadcrumbs
    '.MuiBreadcrumbs-root': {
      marginBottom: theme.spacing(2)
    },
    // Buttons
    '.MuiTab-wrapper, .MuiFab-label': {
      textTransform: 'none'
    },
    '.MuiButton-text': {
      '&.edge-start': {
        marginLeft: theme.spacing(-1)
      },
      '&.edge-end': {
        marginRight: theme.spacing(-1)
      }
    },
    '.MuiButton-root': {
      '&.MuiButton-containedPrimary': {
        '&:focus': {
          outline: '1px dashed',
          outlineColor: styles.fade(theme.palette.common.black, 0.87),
          outlineOffset: '1px'
        }
      }
    },
    // Avatar button
    '.MuiIconButton-root > .MuiIconButton-label > .MuiAvatar-root': {
      width: theme.spacing(4),
      height: theme.spacing(4),
      fontSize: theme.typography.caption.fontSize,
      fontWeight: theme.typography.caption.fontWeight,
      letterSpacing: theme.typography.caption.letterSpacing,
      margin: theme.spacing(-0.5)
    },
    '.MuiIconButton-root.Mui-disabled > .MuiIconButton-label > .MuiAvatar-root': {
      color: styles.fade(theme.palette.common.black, 0.26),
      backgroundColor: styles.fade(theme.palette.common.black, 0.08)
    },
    // Alerts
    '.MuiAlert-action .MuiIconButton-root': {
      color: 'inherit'
    },
    '.MuiAlert-standardError .MuiAlert-action .MuiIconButton-root:hover': {
      backgroundColor: styles.fade(styles.darken(theme.palette.error.main, 0.6), 0.12),
      color: styles.darken(styles.darken(theme.palette.error.main, 0.6), 0.12)
    },
    '.MuiAlert-standardError .MuiAlert-action .MuiIconButton-root:focus': {
      backgroundColor: styles.fade(styles.darken(theme.palette.error.main, 0.6), 0.12),
      color: styles.darken(styles.darken(theme.palette.error.main, 0.6), 0.12)
    },
    '.MuiAlert-standardWarning .MuiAlert-action .MuiIconButton-root:hover': {
      backgroundColor: styles.fade(styles.darken(theme.palette.warning.main, 0.6), 0.12),
      color: styles.darken(styles.darken(theme.palette.warning.main, 0.6), 0.12)
    },
    '.MuiAlert-standardWarning .MuiAlert-action .MuiIconButton-root:focus': {
      backgroundColor: styles.fade(styles.darken(theme.palette.warning.main, 0.6), 0.12),
      color: styles.darken(styles.darken(theme.palette.warning.main, 0.6), 0.12)
    },
    '.MuiAlert-standardInfo .MuiAlert-action .MuiIconButton-root:hover': {
      backgroundColor: styles.fade(styles.darken(theme.palette.info.main, 0.6), 0.12),
      color: styles.darken(styles.darken(theme.palette.info.main, 0.6), 0.12)
    },
    '.MuiAlert-standardInfo .MuiAlert-action .MuiIconButton-root:focus': {
      backgroundColor: styles.fade(styles.darken(theme.palette.info.main, 0.6), 0.12),
      color: styles.darken(styles.darken(theme.palette.info.main, 0.6), 0.12)
    },
    '.MuiAlert-standardSuccess .MuiAlert-action .MuiIconButton-root:hover': {
      backgroundColor: styles.fade(styles.darken(theme.palette.success.main, 0.6), 0.12),
      color: styles.darken(styles.darken(theme.palette.success.main, 0.6), 0.12)
    },
    '.MuiAlert-standardSuccess .MuiAlert-action .MuiIconButton-root:focus': {
      backgroundColor: styles.fade(styles.darken(theme.palette.success.main, 0.6), 0.12),
      color: styles.darken(styles.darken(theme.palette.success.main, 0.6), 0.12)
    },
    '.MuiAlert-action .MuiIconButton-root:hover': {
      backgroundColor: styles.fade(theme.palette.common.black, 0.12)
    },
    '.MuiAlert-action .MuiIconButton-root:focus': {
      backgroundColor: styles.fade(theme.palette.common.black, 0.12)
    },
    // Accordions
    '.MuiAccordion-root': {
      'borderRadius': [0, '!important'],
      '&::before': {
        display: 'none'
      },
      '&.MuiAccordion-root.Mui-expanded': {
        margin: 0
      },
      'boxShadow': 'none',
      'borderBottom': '1px solid',
      'borderBottomColor': theme.palette.divider,
      '&:last-child': {
        borderBottom: 'none'
      }
    },
    '.MuiAccordionSummary-root': {
      padding: 0
    },
    '.MuiAccordionSummary-content, .MuiAccordionSummary-content.Mui-expanded': {
      margin: theme.spacing(3, 0)
    },
    '.MuiAccordionDetails-root': {
      padding: theme.spacing(0, 0, 3, 0)
    },
    // Textfields
    '.MuiOutlinedInput-notchedOutline': {
      borderColor: 'rgba(0, 0, 0, 0.42)'
    },
    // Dense
    '.MuiFormControl-marginDense': {
      'margin': theme.spacing(0, 0, 0, 0),
      // Outlined label
      '& .MuiInputLabel-outlined': {
        'fontSize': theme.typography.body2.fontSize,
        'letterSpacing': theme.typography.body2.letterSpacing,
        'transform': 'translate(14px, 14px) scale(1)',
        '&.MuiInputLabel-shrink': {
          'transform': 'translate(14px, -5px) scale(0.857)',
          'letterSpacing': theme.typography.caption.letterSpacing,
          '&+ .MuiOutlinedInput-root': {
            '& > fieldset > legend ': {
              fontSize: theme.typography.caption.fontSize,
              letterSpacing: theme.typography.caption.letterSpacing
            }
          }
        }
      },
      // Outlined input
      '& .MuiOutlinedInput-root': {
        'fontSize': theme.typography.body2.fontSize,
        'letterSpacing': theme.typography.body2.letterSpacing,
        '& input.MuiOutlinedInput-inputMarginDense': {
          'paddingTop': theme.spacing(1.625),
          'paddingBottom': theme.spacing(1.625),
          '&:not(.MuiInputBase-inputMultiline)': {
            height: '1em'
          }
        },
        // Select
        '& .MuiSelect-selectMenu': {
          height: '1em',
          minHeight: 0,
          paddingTop: theme.spacing(1.625),
          paddingBottom: theme.spacing(1.625)
        },
        '& .MuiSelect-icon': {
          color: theme.palette.buttons.default
        },
        // Autocomplete
        '&.MuiAutocomplete-inputRoot': {
          'paddingTop': theme.spacing(0),
          'paddingBottom': theme.spacing(0),
          'paddingLeft': theme.spacing(0),
          '& .MuiAutocomplete-clearIndicatorDirty': {
            visibility: 'visible'
          },
          '& .MuiAutocomplete-input': {
            paddingTop: theme.spacing(1.625),
            paddingRight: theme.spacing(1.75),
            paddingBottom: theme.spacing(1.625),
            paddingLeft: theme.spacing(1.75)
          }
        },
        // Placeholder
        '& .MuiInputBase-input::placeholder': {
          fontSize: theme.typography.body2.fontSize,
          letterSpacing: theme.typography.body2.letterSpacing
        },
        // Input adornments
        '& .MuiInputAdornment-root': {
          'color': theme.palette.grey[500],
          '& .MuiTypography-root': {
            fontSize: theme.typography.body2.fontSize,
            letterSpacing: theme.typography.body2.letterSpacing
          }
        }
      },
      // Outlined multiline input
      '& .MuiOutlinedInput-multiline': {
        paddingTop: theme.spacing(1.625),
        paddingBottom: theme.spacing(1.625)
      }
    },
    // Radio buttons & checkboxes
    'legend.MuiFormLabel-root:not(.MuiInputLabel-root)': {
      lineHeight: theme.typography.body2.lineHeight,
      fontSize: theme.typography.body2.fontSize,
      letterSpacing: theme.typography.body2.letterSpacing,
      color: theme.palette.text.primary,
      marginBottom: theme.spacing(1)
    },
    'legend.MuiFormLabel-root:not(.MuiInputLabel-root) + .MuiFormHelperText-root, .MuiCheckbox-root + .MuiFormControlLabel-label > .MuiFormHelperText-root': {
      marginTop: theme.spacing(-1),
      marginBottom: theme.spacing(1),
      marginLeft: 0,
      marginRight: 0
    },
    'legend.MuiFormLabel-root:not(.MuiInputLabel-root).Mui-focused': {
      color: theme.palette.text.primary
    },
    'legend.MuiFormLabel-root:not(.MuiInputLabel-root).Mui-error': {
      'color': theme.palette.error.main,
      '& +.MuiFormGroup-root': {
        '& .MuiRadio-root, & .MuiCheckbox-root': {
          color: [theme.palette.error.main, '!important']
        }
      }
    },
    '.MuiFormHelperText-root.Mui-error': {
      '& +.MuiFormGroup-root': {
        '& .MuiRadio-root, & .MuiCheckbox-root': {
          color: [theme.palette.error.main, '!important']
        }
      }
    },
    '.MuiFormGroup-root': {
      '& .MuiFormControlLabel-root': {
        'marginLeft': theme.spacing(-1),
        'marginRight': theme.spacing(0),
        'alignItems': 'start',
        '&:not(.Mui-disabled)': {
          '&:hover': {
            '& .MuiRadio-colorPrimary, & .MuiCheckbox-colorPrimary': {
              color: theme.palette.primary.dark,
              backgroundColor: styles.fade(theme.palette.primary.main, 0.12)
            }
          }
        }
      },
      '& .MuiFormControlLabel-label': {
        'fontSize': theme.typography.body2.fontSize,
        'letterSpacing': theme.typography.body2.letterSpacing,
        'padding': theme.spacing(1.25, 0, 1, 1),
        '& .MuiFormHelperText-root': {
          marginTop: theme.spacing(0)
        }
      }
    },
    '.MuiRadio-root, .MuiCheckbox-root': {
      color: theme.palette.buttons.default,
      padding: theme.spacing(1)
    },
    // Fix for bug: extra space under text field with long label in dialogs
    '.MuiOutlinedInput-root > fieldset > legend > span': {
      display: 'none'
    },
    '.MuiInputLabel-outlined.MuiInputLabel-shrink + .MuiOutlinedInput-root': {
      '& > fieldset > legend > span': {
        display: 'inline-block'
      }
    },
    // Fix for bug: fieldset overflowing container in IE11
    'fieldset.MuiFormControl-root': {
      maxWidth: '100%'
    },
    '.input-margin': {
      'marginBottom': theme.spacing(3),
      'marginTop': 0,
      '&:last-child': {
        marginBottom: 0
      }
    },
    '.radio-margin': {
      'marginBottom': theme.spacing(2),
      'marginTop': 0,
      '&:last-child': {
        marginBottom: 0
      }
    },
    '.emphasisBox': {
      background: theme.palette.grey[200],
      padding: theme.spacing(2),
      marginBottom: theme.spacing(2),
      borderLeftStyle: 'solid',
      borderLeftWidth: '5px',
      borderLeftColor: theme.palette.primary.main
    },
    // Links
    '.MuiLink-root:focus': {
      outline: '1px dashed'
    },
    // Bullet lists
    'li::marker': {
      fontFamily: ['"Roboto"', 'sans-serif']
    },
    // Lists & Menus
    '.MuiMenuItem-root': {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
      minHeight: theme.spacing(0)
    },
    '.MuiMenuItem-root > a': {
      padding: theme.spacing(1, 2),
      margin: theme.spacing(-1, -2),
      display: 'flex',
      width: '100%',
      overflow: 'hidden',
      color: 'inherit',
      textDecoration: ['none', '!important']
    },
    '.MuiListItemText-root': {
      marginTop: theme.spacing(0),
      marginBottom: theme.spacing(0)
    },
    '.MuiListItemIcon-root': {
      minWidth: theme.spacing(0),
      marginTop: '-2px',
      marginBottom: '-2px'
    },
    '.MuiListItemText-root + .MuiListItemIcon-root': {
      marginLeft: theme.spacing(1)
    },
    '.MuiListItemIcon-root + .MuiListItemText-root': {
      marginLeft: theme.spacing(1)
    },
    // Datepickers
    '.MuiPickersToolbarText-toolbarTxt': {
      color: theme.palette.common.white
    },
    '.MuiPickersCalendarHeader-dayLabel': {
      color: theme.palette.grey[600]
    },
    '.MuiPickersCalendarHeader-transitionContainer': {
      height: '1.5em'
    },
    // Tables
    '.MuiTableCell-root': {
      padding: theme.spacing(1)
    },
    '.MuiTableCell-stickyHeader': {
      backgroundColor: 'white'
    },

    /*
    Commented out for now, will revisit at a later date to address tabbing vs hover bg color
    '.MuiTableRow-root.Mui-selected, .MuiTableRow-root.Mui-selected:hover': {
      backgroundColor: fade(theme.palette.primary.main),
    },
    },*/
    '.MuiTableSortLabel-root:focus .MuiTableSortLabel-icon': {
      opacity: 0.5
    },
    // Typography
    '.MuiTypography-gutterBottom': {
      marginBottom: '0.5em'
    },
    // Pagination
    '.MuiPaginationItem-root': {
      margin: '0px'
    },
    '.MuiPaginationItem-page': {
      height: '2.5rem',
      minWidth: '2.5rem',
      borderRadius: '24px'
    },
    // Dialogs
    '.MuiDialog-paperWidthSm, .MuiDialog-paperWidthMd': {
      'width': 'calc(100% - 64px)',
      '& .MuiDialogContent-root': {
        padding: 0
      },
      '& .MuiDialogActions-spacing': {
        'padding': theme.spacing(1.75, 3),
        '&> :not(:first-child)': {
          marginLeft: theme.spacing(2)
        }
      },
      '& .MuiFormLabel-root': {
        'background-color': 'white'
      },
      '& .MuiOutlinedInput-multiline': {
        padding: 0
      },
      '& .MuiOutlinedInput-inputMultiline': {
        'max-height': 130,
        'overflow': 'auto !important',
        'padding': theme.spacing(2)
      }
    },
    // *********** Custom styles **************
    '.avatar-orange': {
      color: theme.palette.getContrastText(colors.deepOrange[500]),
      backgroundColor: colors.deepOrange[500]
    },
    '.avatar-green': {
      color: theme.palette.getContrastText(colors.green[800]),
      backgroundColor: colors.green[800]
    },
    '.avatar-purple': {
      color: theme.palette.getContrastText(colors.deepPurple[500]),
      backgroundColor: colors.deepPurple[500]
    },
    '.btn-edge-end': {
      marginRight: theme.spacing(-1)
    },
    '.btn-edge-start': {
      marginLeft: theme.spacing(-1)
    },
    '.form-control': {
      display: 'block',
      marginBottom: theme.spacing(3)
    },
    '.grey-section': {
      margin: theme.spacing(0, -4),
      padding: theme.spacing(4, 4),
      backgroundColor: theme.palette.grey[100]
    },
    '.heading-underline': {
      width: '100%',
      borderBottomStyle: 'solid',
      borderBottomWidth: '1px',
      borderBottomColor: theme.palette.grey[300]
    },
    '.help-btn': {
      textTransform: 'none',
      zIndex: '2000',
      position: 'fixed',
      top: '93vh',
      left: '84vw'
    },
    '.icon-grey': {
      color: theme.palette.grey[600],
      fill: theme.palette.grey[600]
    },
    '.list-horizontal': {
      'display': 'flex',
      'flex-wrap': 'wrap',
      'alignItems': 'center',
      'padding': 0,
      '& li': {
        display: 'inline-block'
      }
    },
    '.page-container': _defineProperty({
      marginTop: theme.spacing(8),
      padding: theme.spacing(0, 2)
    }, theme.breakpoints.down('sm'), {
      marginTop: theme.spacing(16)
    }),
    '.paper-grey': {
      padding: theme.spacing(2),
      backgroundColor: theme.palette.grey[100],
      borderColor: styles.fade(theme.palette.common.black, 0.08)
    },
    '.paper-heading': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: theme.palette.grey[100],
      margin: theme.spacing(-2, -2, 2, -2),
      padding: theme.spacing(1, 2)
    },
    '.phone-num-lg': {
      fontSize: '1.5rem',
      fontWeight: 300
    },
    '.resp-iframe-container': {
      position: 'relative',
      overflow: 'hidden',
      paddingTop: '56.25%'
    },
    '.resp-iframe': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      border: 0
    },
    '.screen-reader-text': {
      clip: 'rect(1px, 1px, 1px, 1px)',
      height: '1px',
      margin: 0,
      overflow: 'hidden',
      position: 'absolute',
      width: '1px'
    },
    'section': {
      marginBottom: theme.spacing(6)
    },
    '.section-divider': {
      marginBottom: theme.spacing(6)
    },
    '.tab-badge': {
      'display': 'flex',
      '& .MuiChip-root': {
        marginLeft: theme.spacing(1)
      },
      '& .MuiBadge-root': {
        'marginTop': theme.spacing(1.3),
        'marginLeft': theme.spacing(3),
        '& .MuiBadge-badge': {
          'backgroundColor': '#e0e0e0',
          'color': 'rgba(0,0,0,0.87)',
          'display': 'block',
          'padding': '4px 6px',
          'height': 'auto',
          '&.MuiBadge-colorPrimary': {
            color: '#fff',
            backgroundColor: theme.palette.primary.main
          }
        }
      }
    },
    '.tabs-underline': {
      borderBottomColor: theme.palette.grey[300],
      borderBottomStyle: 'solid',
      borderBottomWidth: '1px'
    },
    '.toggle-buttons': {
      '& button': {
        'justifyContent': 'flex-start',
        'margin': theme.spacing(0, 4, 2, 0),
        'borderRadius': 0,
        'minWidth': 0,
        'color': theme.palette.grey[600],
        'transition': 'border-left .1s',
        '&:hover': {
          borderBottomWidth: '2px',
          borderBottomStyle: 'solid',
          borderBottomColor: theme.palette.primary.main,
          color: [theme.palette.common.black, '!important']
        }
      }
    },
    '.toggle-buttons .selected': {
      borderBottomWidth: '2px',
      borderBottomStyle: 'solid',
      borderBottomColor: theme.palette.primary.main,
      color: [theme.palette.common.black, '!important']
    },
    '.vertical-toggle-buttons': {
      'display': 'flex',
      'flexDirection': 'column',
      'alignItems': 'flex-start',
      '& button': {
        'justifyContent': 'flex-start',
        'margin': theme.spacing(2, 0, 2, 0),
        'padding': theme.spacing(0, 0, 0, 2),
        'borderRadius': 0,
        'minWidth': 0,
        'color': theme.palette.grey[600],
        'transition': 'border-left .1s',
        'width': '50%',
        '&:hover': {
          borderLeftWidth: '2px',
          borderLeftStyle: 'solid',
          borderLeftColor: theme.palette.primary.main,
          paddingLeft: '14px',
          color: [theme.palette.common.black, '!important']
        }
      }
    },
    '.vertical-toggle-buttons .selected': {
      borderLeftWidth: '2px',
      borderLeftStyle: 'solid',
      borderLeftColor: theme.palette.primary.main,
      paddingLeft: '14px',
      color: [theme.palette.common.black, '!important']
    },
    '.row': {
      'display': 'flex',
      'margin': theme.spacing(1.5, 0),
      'flexFlow': 'row',
      'height': 'auto',
      'justifyContent': 'space-between',
      'width': '100%',
      'alignItems': 'center',
      '&:first-child': {
        marginTop: 0
      },
      '&:last-child': {
        marginBottom: 0
      }
    },
    '.column': {
      'display': 'flex',
      'flexDirection': 'column',
      'width': '100%',
      'justifyContent': 'center',
      'marginRight': theme.spacing(1),
      'height': '100%',
      '&:last-child': {
        marginRight: 0
      }
    },
    '.border-top': {
      borderTop: '1px solid',
      borderTopColor: theme.palette.divider
    },
    '.border-bottom': {
      borderBottom: '1px solid',
      borderBottomColor: theme.palette.divider
    },
    // Margin
    '.m-6': {
      margin: theme.spacing(6)
    },
    '.m-5': {
      margin: theme.spacing(5)
    },
    '.m-4': {
      margin: theme.spacing(4)
    },
    '.m-3': {
      margin: theme.spacing(3)
    },
    '.m-2': {
      margin: theme.spacing(2)
    },
    '.m-1': {
      margin: theme.spacing(1)
    },
    '.m-0': {
      margin: theme.spacing(0)
    },
    // Margin top
    '.mt-6': {
      marginTop: theme.spacing(6)
    },
    '.mt-5': {
      marginTop: theme.spacing(5)
    },
    '.mt-4': {
      marginTop: theme.spacing(4)
    },
    '.mt-3': {
      marginTop: theme.spacing(3)
    },
    '.mt-2': {
      marginTop: theme.spacing(2)
    },
    '.mt-1': {
      marginTop: theme.spacing(1)
    },
    '.mt-0': {
      marginTop: theme.spacing(0)
    },
    '.n-mt-1': {
      marginTop: theme.spacing(-1)
    },
    '.n-mt-2': {
      marginTop: theme.spacing(-2)
    },
    '.n-mt-3': {
      marginTop: theme.spacing(-3)
    },
    '.n-mt-4': {
      marginTop: theme.spacing(-4)
    },
    '.n-mt-5': {
      marginTop: theme.spacing(-5)
    },
    '.n-mt-6': {
      marginTop: theme.spacing(-6)
    },
    // Margin bottom
    '.mb-6': {
      marginBottom: theme.spacing(6)
    },
    '.mb-5': {
      marginBottom: theme.spacing(5)
    },
    '.mb-4': {
      marginBottom: theme.spacing(4)
    },
    '.mb-3': {
      marginBottom: theme.spacing(3)
    },
    '.mb-2': {
      marginBottom: theme.spacing(2)
    },
    '.mb-1': {
      marginBottom: theme.spacing(1)
    },
    '.mb-0': {
      marginBottom: theme.spacing(0)
    },
    '.n-mb-1': {
      marginBottom: theme.spacing(-1)
    },
    '.n-mb-2': {
      marginBottom: theme.spacing(-2)
    },
    '.n-mb-3': {
      marginBottom: theme.spacing(-3)
    },
    '.n-mb-4': {
      marginBottom: theme.spacing(-4)
    },
    '.n-mb-5': {
      marginBottom: theme.spacing(-5)
    },
    '.n-mb-6': {
      marginBottom: theme.spacing(-6)
    },
    // Margin right
    '.mr-6': {
      marginRight: theme.spacing(6)
    },
    '.mr-5': {
      marginRight: theme.spacing(5)
    },
    '.mr-4': {
      marginRight: theme.spacing(4)
    },
    '.mr-3': {
      marginRight: theme.spacing(3)
    },
    '.mr-2': {
      marginRight: theme.spacing(2)
    },
    '.mr-1': {
      marginRight: theme.spacing(1)
    },
    '.mr-0': {
      marginRight: theme.spacing(0)
    },
    '.n-mr-1': {
      marginRight: theme.spacing(-1)
    },
    '.n-mr-2': {
      marginRight: theme.spacing(-2)
    },
    '.n-mr-3': {
      marginRight: theme.spacing(-3)
    },
    '.n-mr-4': {
      marginRight: theme.spacing(-4)
    },
    '.n-mr-5': {
      marginRight: theme.spacing(-5)
    },
    '.n-mr-6': {
      marginRight: theme.spacing(-6)
    },
    // Margin left
    '.ml-6': {
      marginLeft: theme.spacing(6)
    },
    '.ml-5': {
      marginLeft: theme.spacing(5)
    },
    '.ml-4': {
      marginLeft: theme.spacing(4)
    },
    '.ml-3': {
      marginLeft: theme.spacing(3)
    },
    '.ml-2': {
      marginLeft: theme.spacing(2)
    },
    '.ml-1': {
      marginLeft: theme.spacing(1)
    },
    '.ml-0': {
      marginLeft: theme.spacing(0)
    },
    '.n-ml-1': {
      marginLeft: theme.spacing(-1)
    },
    '.n-ml-2': {
      marginLeft: theme.spacing(-2)
    },
    '.n-ml-3': {
      marginLeft: theme.spacing(-3)
    },
    '.n-ml-4': {
      marginLeft: theme.spacing(-4)
    },
    '.n-ml-5': {
      marginLeft: theme.spacing(-5)
    },
    '.n-ml-6': {
      marginLeft: theme.spacing(-6)
    },
    // Padding
    '.p-6': {
      padding: theme.spacing(6)
    },
    '.p-5': {
      padding: theme.spacing(5)
    },
    '.p-4': {
      padding: theme.spacing(4)
    },
    '.p-3': {
      padding: theme.spacing(3)
    },
    '.p-2': {
      padding: theme.spacing(2)
    },
    '.p-1': {
      padding: theme.spacing(1)
    },
    '.p-0': {
      padding: theme.spacing(0)
    },
    // Padding top
    '.pt-6': {
      paddingTop: theme.spacing(6)
    },
    '.pt-5': {
      paddingTop: theme.spacing(5)
    },
    '.pt-4': {
      paddingTop: theme.spacing(4)
    },
    '.pt-3': {
      paddingTop: theme.spacing(3)
    },
    '.pt-2': {
      paddingTop: theme.spacing(2)
    },
    '.pt-1': {
      paddingTop: theme.spacing(1)
    },
    '.pt-0': {
      paddingTop: theme.spacing(0)
    },
    // Padding bottom
    '.pb-6': {
      paddingBottom: theme.spacing(6)
    },
    '.pb-5': {
      paddingBottom: theme.spacing(5)
    },
    '.pb-4': {
      paddingBottom: theme.spacing(4)
    },
    '.pb-3': {
      paddingBottom: theme.spacing(3)
    },
    '.pb-2': {
      paddingBottom: theme.spacing(2)
    },
    '.pb-1': {
      paddingBottom: theme.spacing(1)
    },
    '.pb-0': {
      paddingBottom: theme.spacing(0)
    },
    // Padding right
    '.pr-6': {
      paddingRight: theme.spacing(6)
    },
    '.pr-5': {
      paddingRight: theme.spacing(5)
    },
    '.pr-4': {
      paddingRight: theme.spacing(4)
    },
    '.pr-3': {
      paddingRight: theme.spacing(3)
    },
    '.pr-2': {
      paddingRight: theme.spacing(2)
    },
    '.pr-1': {
      paddingRight: theme.spacing(1)
    },
    '.pr-0': {
      paddingRight: theme.spacing(0)
    },
    // Padding left
    '.pl-6': {
      paddingLeft: theme.spacing(6)
    },
    '.pl-5': {
      paddingLeft: theme.spacing(5)
    },
    '.pl-4': {
      paddingLeft: theme.spacing(4)
    },
    '.pl-3': {
      paddingLeft: theme.spacing(3)
    },
    '.pl-2': {
      paddingLeft: theme.spacing(2)
    },
    '.pl-1': {
      paddingLeft: theme.spacing(1)
    },
    '.pl-0': {
      paddingLeft: theme.spacing(0)
    }
  }
});

exports.Card = Card;
exports.Dialog = Dialog;
exports.Drawer = Drawer;
exports.Snackbar = Snackbar;
exports.darkTheme = darkTheme;
exports.styles = useStyles;
exports.theme = theme;
