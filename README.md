# Data Analytics as a Service Design System


## Installation 
```
npm i daaas-design-system @material-ui/styles
```

## Usage
```javascript
import React from "react";
import ReactDOM from "react-dom";
import { Card } from "daaas-design-system";
import { styles, theme } from "daaas-design-system";
import { ThemeProvider } from "@material-ui/styles";

function App() {
  styles();

  return (
    <ThemeProvider theme={theme}>
      <Card
        title="Card title"
        error={false}
        primaryButton="Edit"
        secondaryButton="Delete"
        content={<span>Hello world</span>}
      />
    </ThemeProvider>
  );
}

ReactDOM.render(<App />, document.querySelector('#app'));

```



